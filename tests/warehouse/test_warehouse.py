import pytest

from warehouse_system.warehouse import Warehouse



def create_simple_warehouse():
    """a simple example wareshouse:

    +---+---+---+
    |           |
    +   +---+   |
    |           |
    +---+---+---+
    """
    wh = Warehouse()
    wh.add_passway('A1', 'A2')
    wh.add_passway('A2', 'A3')
    wh.add_passway('B1', 'B2')
    wh.add_passway('B2', 'B3')
    wh.add_passway('A1', 'B1')
    wh.add_passway('A3', 'B3')
    return wh



def test_check_transition():
    wh = create_simple_warehouse()
    assert wh.check_transition('A1', 'A2')
    assert not wh.check_transition('A2', 'A2')


def test_capacity():
    wh = Warehouse()
    wh.set_capacity('A3', 11)
    assert wh.get_capacity('A3') == 11


def test_overall_capacity():
    wh = Warehouse()
    wh.set_capacity('A1', 22)
    wh.set_capacity('B1', 20)
    assert wh.overall_capacity() == 42


def test_shortest_path():
    wh = create_simple_warehouse()
    assert wh.shortest_path('A1', 'A2') == ['A1', 'A2' ]
    assert wh.shortest_path('A2', 'B3') == ['A2', 'A3', 'B3' ]
