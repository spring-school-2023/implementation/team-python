from warehouse_system.requester import Requester

class Response():
    def __init__(self):
        self.dict_ = dict()
    
    def json(self):
        return self.dict_

class MockRequester(Requester):
    def __init__(self):
        self.response_ = Response()

    def get(self, url, params=None, **kwargs):
        self.url_ = url
        return self.response_

    def post(self, url, data=None, json=None, **kwargs):
        self.url_ = url
        return self.response_

    def put(self, url, data=None, **kwargs):
        self.url_ = url
        return self.response_