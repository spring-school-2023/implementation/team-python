import pytest
import networkx as nx

from warehouse_system.warehouse_plotter import WarehousePlotter
from test_cases_graph_to_nodes import test_cases_graph_to_nodes

@pytest.mark.parametrize("test_case", test_cases_graph_to_nodes)
def test_warehouse_plotter_graph_to_nodes_test_cases(test_case):
    under_test = WarehousePlotter()

    input_graph, expected_response = test_case
    assert expected_response == under_test.graph_to_nodes(input_graph)