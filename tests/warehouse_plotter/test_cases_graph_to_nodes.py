import networkx as nx

# Most simple graph
one_node_graph = nx.Graph()
one_node_graph.add_nodes_from([
        ("0:0", {"capacity": 4}),
    ])

one_node_graph_expected = {
        "0:0": {
            "north": True,
            "east": True,
            "south": True,
            "west": True
        }}


# Graph with two nodes (horizontal)
two_node_graph1 = nx.Graph()
two_node_graph1.add_nodes_from([
    ("0:0", {"capacity": 12}),
    ("1:0", {"capacity": 12}),
])
two_node_graph1.add_edges_from([
    ("0:0","1:0")
])

two_node_graph1_expected = {
        "0:0": {
            "north": True,
            "east": False,
            "south": True,
            "west": True
        },
        "1:0": {
            "north": True,
            "east": True,
            "south": True,
            "west": False
        }}


# Graph with two nodes (vertical)
two_node_graph2 = nx.Graph()
two_node_graph2.add_nodes_from([
    ("0:0", {"capacity": 12}),
    ("0:1", {"capacity": 12}),
])
two_node_graph2.add_edges_from([
    ("0:0","0:1")
])

two_node_graph2_expected = {
        "0:0": {
            "north": True,
            "east": True,
            "south": False,
            "west": True
        },
        "0:1": {
            "north": False,
            "east": True,
            "south": True,
            "west": True
        }}

# Graph with tree nodes and neg. indexes
three_node_graph_with_negative_index = nx.Graph()
three_node_graph_with_negative_index = nx.Graph()
three_node_graph_with_negative_index.add_nodes_from([
    ("0:0", {"capacity": 9}),
    ("-1:0", {"capacity": 12}),
    ("0:-1", {"capacity": 12}),
])
three_node_graph_with_negative_index.add_edges_from([
    ("0:0","-1:0"),
    ("0:0","0:-1"),
])

three_node_graph_with_negative_index_expected = {
        "0:0": {
            "north": False,
            "east": True,
            "south": True,
            "west": False
        },
        "0:-1": {
            "north": True,
            "east": True,
            "south": False,
            "west": True
        },
        "-1:0": {
            "north": True,
            "east": False,
            "south": True,
            "west": True
        }}

test_cases_graph_to_nodes_graphs = [one_node_graph, two_node_graph1, two_node_graph2, three_node_graph_with_negative_index]
test_cases_graph_to_nodes_expected = [one_node_graph_expected, two_node_graph1_expected, two_node_graph2_expected, three_node_graph_with_negative_index_expected]

test_cases_graph_to_nodes = list(zip(test_cases_graph_to_nodes_graphs, test_cases_graph_to_nodes_expected))