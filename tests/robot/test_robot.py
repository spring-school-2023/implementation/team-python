import pytest


from warehouse_system.robot import Robot, Direction
from warehouse_system.warehouse import Warehouse
from tests.mocks.mock_requester import MockRequester


TEST_API_URL = "test_api_url/"
TEST_SESSION_ID = "test_session_id"


def generateTestWarehouse():
    wh = Warehouse()
    wh.add_passway('0:0', '-1:0')
    wh.add_passway('0:0', '0:-1')
    wh.add_passway('0:0', '0:1')
    wh.add_passway('0:0', '1:0')
    return wh


def test_robot_move_wrong_input_return_error_response():
    mock_requester = MockRequester()
    under_test = Robot(mock_requester, TEST_API_URL, TEST_SESSION_ID)
    under_test.warehouse = generateTestWarehouse()

    expected_response = {'message': 'wrong_command', 'request_status': 'failed'}
    assert expected_response == under_test.move("blub")


@pytest.mark.parametrize("direction", ["north", "east", "south", "west"])
def test_move_direction_correct_url(direction):
    mock_requester = MockRequester()
    under_test = Robot(mock_requester, TEST_API_URL, TEST_SESSION_ID)
    under_test.warehouse = generateTestWarehouse()
    under_test.position = {"x": 0, "y": 0}
    mock_requester.response_.dict_ = {"message": "done", "request_status": "ok"}
    under_test.move(direction)
    expected_response = f"{TEST_API_URL}api/bot/{TEST_SESSION_ID}/move/{direction}"
    assert expected_response == mock_requester.url_


@pytest.mark.parametrize("direction", ["north", "east", "south", "west"])
def test_move_direction_should_return_ok(direction):
    mock_requester = MockRequester()
    under_test = Robot(mock_requester, TEST_API_URL, TEST_SESSION_ID)
    under_test.warehouse = generateTestWarehouse()
    under_test.position = {"x": 0, "y": 0}

    expected_response = {"message": "done", "request_status": "ok"}
    mock_requester.response_.dict_ = expected_response

    assert expected_response == under_test.move(direction)


def test_reset_state_correct_url():
    mock_requester = MockRequester()
    under_test = Robot(mock_requester, TEST_API_URL, TEST_SESSION_ID)
    under_test.warehouse = generateTestWarehouse()
    under_test.position = {"x": 0, "y": 0}

    mock_requester.response_.dict_ = {"message": "done", "request_status": "ok"}
    under_test.resetState()
    expected_response = f"{TEST_API_URL}api/james/{TEST_SESSION_ID}/reset"
    assert expected_response == mock_requester.url_


def test_reset_state_should_return_ok():
    mock_requester = MockRequester()
    under_test = Robot(mock_requester, TEST_API_URL, TEST_SESSION_ID)
    under_test.warehouse = generateTestWarehouse()
    under_test.position = {"x": 0, "y": 0}

    expected_response = {"message": "done", "request_status": "ok"}
    mock_requester.response_.dict_ = expected_response

    assert expected_response == under_test.resetState()


def test_reset_state_should_reset_position():
    mock_requester = MockRequester()
    under_test = Robot(mock_requester, TEST_API_URL, TEST_SESSION_ID)
    under_test.warehouse = generateTestWarehouse()

    mock_requester.response_.dict_ = {"message": "done", "request_status": "ok"}
    under_test.resetState()

    expected_response = {"x": 7, "y": 1}

    assert expected_response == under_test.position


@pytest.mark.parametrize("direction", ["north", "east", "south", "west"])
def test_move_direction_correct_offset(direction):
    mock_requester = MockRequester()
    under_test = Robot(mock_requester, TEST_API_URL, TEST_SESSION_ID)
    under_test.warehouse = generateTestWarehouse()
    under_test.position = {"x": 0, "y": 0}
    mock_requester.response_.dict_ = {"message": "done", "request_status": "ok"}
    under_test.move(direction)

    expected_response = {"x": Direction(direction).offset_x,
                         "y": Direction(direction).offset_y}

    assert expected_response == under_test.position


def test_reset_state_should_save_and_load():
    mock_requester = MockRequester()
    under_test = Robot(mock_requester, TEST_API_URL, TEST_SESSION_ID)
    under_test.warehouse = generateTestWarehouse()
    under_test.position = {"x": 1, "y": 2}

    under_test.save("tmpFile")

    expected_response = under_test.position

    under_test_2 = Robot(mock_requester, TEST_API_URL, TEST_SESSION_ID)
    under_test_2 = under_test_2.load("tmpFile")

    assert expected_response == under_test_2.position
