# Importing the library
import pygame
import networkx as nx


class WarehousePlotter():
    def __init__(self):
        self.canvas_size = 800
        self.tile_size = 50
        self.offset = self.canvas_size/2 - self.tile_size/2

        self.line_thickness = 2

        # Initializing Color
        self.color_orange = (255, 0, 0)
        self.color_blue = (0, 145, 220)
        self.color_grey = (182, 190, 198)
        self.color_dark_grey = (150, 150, 150)

    def uid_to_position(self, uid):
        x = int(uid.split(":")[0])
        y = int(uid.split(":")[1])
        return {"x": x, "y": y}
        
    def graph_to_nodes(self, graph):
        node_dict = dict()
        for node in list(graph.nodes):
            node_dict[node] = {"north": True, "east": True, "south": True, "west": True}

        edge_list = list(graph.edges)
        for node1, node2 in edge_list:
            node1_pos = self.uid_to_position(node1)
            node2_pos = self.uid_to_position(node2)
            if node2_pos["x"] == node1_pos["x"] + 1:
                node_dict[node1]["east"] = False
                node_dict[node2]["west"] = False
            if node2_pos["x"] == node1_pos["x"] - 1:
                node_dict[node1]["west"] = False
                node_dict[node2]["east"] = False
            if node2_pos["y"] == node1_pos["y"] + 1:
                node_dict[node1]["south"] = False
                node_dict[node2]["north"] = False
            if node2_pos["y"] == node1_pos["y"] - 1:
                node_dict[node1]["north"] = False
                node_dict[node2]["south"] = False
        return node_dict

    def draw(self, nodes, robot_uid):
        # Initializing Pygame
        pygame.init()
        # Initializing surface
        self.surface = pygame.display.set_mode((self.canvas_size, self.canvas_size))

        for node in nodes:
            self.draw_tile(node,nodes[node])
        self.draw_robot(robot_uid)

    def draw_tile(self, uid, walls):
        position = self.uid_to_position(uid)

        x = position["x"] * self.tile_size + self.offset
        y = position["y"] * self.tile_size + self.offset

        # Drawing Rectangle
        pygame.draw.rect(self.surface, self.color_grey, pygame.Rect(x, y, self.tile_size, self.tile_size))

        # draw wallbackground
        pygame.draw.line(self.surface, self.color_dark_grey, (x, y), (x + self.tile_size, y), self.line_thickness)
        pygame.draw.line(self.surface, self.color_dark_grey, (x + self.tile_size, y), (x + self.tile_size, y + self.tile_size),
                         self.line_thickness)
        pygame.draw.line(self.surface, self.color_dark_grey, (x, y + self.tile_size), (x + self.tile_size, y + self.tile_size),
                         self.line_thickness)
        pygame.draw.line(self.surface, self.color_dark_grey, (x, y), (x, y + self.tile_size), self.line_thickness)

        # north
        if walls["north"]:
            pygame.draw.line(self.surface, self.color_orange, (x, y), (x + self.tile_size, y), self.line_thickness)
        # east
        if walls["east"]:
            pygame.draw.line(self.surface, self.color_orange, (x + self.tile_size, y), (x + self.tile_size, y + self.tile_size),
                             self.line_thickness)
        # south
        if walls["south"]:
            pygame.draw.line(self.surface, self.color_orange, (x, y + self.tile_size), (x + self.tile_size, y + self.tile_size),
                             self.line_thickness)
        # west
        if walls["west"]:
            pygame.draw.line(self.surface, self.color_orange, (x, y), (x, y + self.tile_size), self.line_thickness)

        pygame.display.flip()
    
    def draw_robot(self, uid):
        position = self.uid_to_position(uid)
        x = position["x"] * self.tile_size + self.offset + self.tile_size/2
        y = position["y"] * self.tile_size + self.offset + self.tile_size/2

        pygame.draw.circle(self.surface, self.color_blue, (x,y), self.tile_size/4, self.line_thickness*2)

    def show(self):
        # infinite loop
        while True:
            # iterate over the list of Event objects
            # that was returned by pygame.event.get() method.
            for event in pygame.event.get():

                # if event object type is QUIT
                # then quitting the pygame
                # and program both.
                if event.type == pygame.QUIT:
                    # deactivates the pygame library
                    pygame.quit()

                    # quit the program.
                    quit()

                # Draws the surface object to the screen. 
                pygame.display.update()




# wd = WarehousePlotter()
# graph = nx.Graph()
# graph.add_nodes_from(["0:0", "1:0","0:1","1:1"])
# graph.add_edges_from([("0:0", "1:0"), ("1:0","1:1")])
# nodes = wd.graph_to_nodes(graph)

# wd.draw(nodes, "1:1")

# wd.show()
