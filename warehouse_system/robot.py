from warehouse_system.warehouse import Warehouse
from .requester import Requester
from enum import Enum
import pickle


class Direction(str, Enum):
    offset_x: int
    offset_y: int

    SOUTH = "south", 0, 1
    NORTH = "north", 0, -1
    EAST = "east", 1, 0
    WEST = "west", -1, 0

    def __new__(cls, id: str, offset_x: int, offset_y: int):
        obj = str.__new__(cls, id)
        obj._value_ = id

        obj.offset_x = offset_x
        obj.offset_y = offset_y
        return obj


class Robot():

    apiURL: str
    requester: Requester
    sessionID: str
    position: dict
    warehouse: Warehouse

    def __init__(self, requester: Requester, apiURL: str, sessionID: str):
        self.requester = requester
        self.apiURL = apiURL
        self.sessionID = sessionID
        self.position = {"x": 7, "y": 1}
        self.warehouse = Warehouse()
        pass

    def resetState(self) -> dict():
        resetResponse = self.requester.put(url=self.apiURL + "api/james/" + self.sessionID + "/reset").json()
        if (resetResponse["request_status"] == "ok"):
            self.position = {"x": 7, "y": 1}
        return resetResponse

    def move(self, direction: str) -> dict():

        if (direction != Direction.NORTH and direction != Direction.EAST and direction != Direction.SOUTH and direction != Direction.WEST):
            return {
                "message": "wrong_command",
                "request_status": "failed"
            }

        newPosition = {
            "x": self.position["x"] + Direction(direction).offset_x,
            "y": self.position["y"] + Direction(direction).offset_y
        }

        print(newPosition)

        if (not self.warehouse.check_transition(str(self.position["x"]) + ":" + str(self.position["y"]), str(newPosition["x"]) + ":" + str(newPosition["y"]))):
            return {
                "message": "wall_detected",
                "request_status": "failed"
            }

        response = self.requester.put(url=self.apiURL + "api/bot/" + self.sessionID + "/move/" + direction).json()

        if (response["request_status"] == "ok"):
            self.position["x"] += Direction(direction).offset_x
            self.position["y"] += Direction(direction).offset_y
        return response

    def save(self, filename):
        file = open(filename, 'wb')
        pickle.dump(self, file)
        file.close()

    def load(self, filename):
        file = open(filename, 'rb')
        self = pickle.load(file)
        file.close()
        return self
