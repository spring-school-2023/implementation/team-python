import networkx as nx
from abc import ABC, abstractmethod
from .warehouse_plotter import WarehousePlotter


class WarehouseABC(ABC):
    @abstractmethod
    def add_passway(self, location1: str, location2: str) -> None:
        """Adds a passway between two locations."""
        pass

    @abstractmethod
    def check_transition(self, location1: str, location2: str) -> bool:
        """Checks if a transition between two locations is possible."""
        pass

    @abstractmethod
    def overall_capacity(self) -> int:
        """Returns the overall capacity of the warehouse."""
        pass

    @abstractmethod
    def get_capacity(self, location: str) -> int:
        """Returns the capacity of a specific location."""
        pass

    @abstractmethod
    def set_capacity(self, location: str, capacity: int) -> None:
        """Sets the capacity of a specific location."""
        pass

    @abstractmethod
    def reset_state(self) -> None:
        """Resets the state of the warehouse to the default state."""
        pass

    @abstractmethod
    def shortest_path(self, location1: str, location2: str) -> list:
        """Returns the shortest path between two locations."""
        pass


class Warehouse(WarehouseABC):
    """
    A class representing a warehouse using a graph.
    """

    layout: nx.Graph  # A graph representing the layout of the warehouse
    capacities: dict[str, int]  # A dictionary of locations and their associated capacity
    plotter: WarehousePlotter

    def __init__(self) -> None:
        """
        Initializes a new Warehouse object.
        """
        self.plotter = WarehousePlotter()
        self.reset_state()

    def add_passway(self, location1: str, location2: str) -> None:
        """
        Adds a passway between two locations.

        Args:
            location1 (str): The first location.
            location2 (str): The second location.
        """
        self.layout.add_edge(location1, location2)

    def check_transition(self, location1: str, location2: str) -> bool:
        """
        Checks if a transition between two locations is possible.

        Args:
            location1 (str): The first location.
            location2 (str): The second location.

        Returns:
            bool: True if the transition is possible, False otherwise.
        """
        return self.layout.has_edge(location1, location2)

    def overall_capacity(self) -> int:
        """
        Returns the overall capacity of the warehouse.

        Returns:
            int: The overall capacity of the warehouse.
        """
        return sum(self.capacities.values())

    def reset_state(self) -> None:
        """
        Resets the state of the warehouse to the default state.
        """
        self.capacities = dict()
        self.layout = nx.Graph()

    def get_capacity(self, location: str) -> int:
        """
        Returns the capacity of a specific location.

        Args:
            location (str): The location.

        Returns:
            int: The capacity of the location.
        """
        return self.capacities.get(location, 0)

    def set_capacity(self, location: str, capacity: int) -> None:
        """
        Sets the capacity of a specific location.

        Args:
            location (str): The location.
            capacity (int): The capacity to set.
        """
        self.capacities[location] = capacity

    def shortest_path(self, location1: str, location2: str) -> list:
        """
        Returns the shortest path between two locations.

        Args:
            location1 (str): The first location.
            location2 (str): The second location.

        Returns:
            list: The shortest path between the two locations.
        """
        return nx.shortest_path(self.layout, source=location1, target=location2)
