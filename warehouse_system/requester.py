import requests

class Requester():
    def get(self, url: str, params=None) -> requests.Response:
        return requests.get(url, params)

    def post(self, url: str, data=None, json=None) -> requests.Response:
        return requests.post(url, data, json)

    def put(self, url: str, data=None) -> requests.Response:
        return requests.put(url, data)
