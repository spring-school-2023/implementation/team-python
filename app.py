from warehouse_system.robot import Robot, Direction
from warehouse_system.warehouse import Warehouse
from warehouse_system.requester import Requester
import argparse
from enum import Enum
import pickle

WAREHOUSE_API_URL = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/"
SESSION_ID = "tessa"


def create_large_warehouse():
    """a larger example wareshouse"""
    wh = Warehouse()
    # row 1
    # wh.add_passway('2:1', '3:1')
    wh.add_passway('3:1', '4:1')
    wh.add_passway('4:1', '5:1')
    wh.add_passway('5:1', '6:1')
    wh.add_passway('6:1', '7:1')
    # wh.add_passway('2:1', '3:1')
    # row 1 to row 2
    wh.add_passway('3:1', '3:2')
    wh.add_passway('6:1', '6:2')
    # row 2
    wh.add_passway('4:2', '5:2')
    wh.add_passway('5:2', '6:2')
    wh.add_passway('6:2', '7:2')
    # row 2 to row 3
    wh.add_passway('3:2', '3:3')
    wh.add_passway('4:2', '4:3')
    wh.add_passway('6:2', '6:3')
    # row 3
    wh.add_passway('1:3', '2:3')
    wh.add_passway('2:3', '3:3')
    wh.add_passway('3:3', '4:3')
    wh.add_passway('4:3', '5:3')
    wh.add_passway('6:3', '7:3')
    # row 3 to row 4
    wh.add_passway('2:3', '2:4')
    wh.add_passway('4:3', '4:4')
    wh.add_passway('5:3', '5:4')
    wh.add_passway('6:3', '6:4')
    wh.add_passway('7:3', '7:4')
    # row 4
    wh.add_passway('1:4', '2:4')
    wh.add_passway('2:4', '3:4')
    # row 4 to row 5
    wh.add_passway('2:4', '2:5')
    wh.add_passway('4:4', '4:5')
    wh.add_passway('5:4', '5:5')
    wh.add_passway('6:4', '6:5')
    wh.add_passway('7:4', '7:5')
    # row 5
    wh.add_passway('1:5', '2:5')
    wh.add_passway('2:5', '3:5')
    wh.add_passway('5:5', '6:5')
    # row 5 to row 6
    wh.add_passway('2:5', '2:6')
    wh.add_passway('4:5', '4:6')
    wh.add_passway('7:5', '7:6')
    # row 6
    wh.add_passway('1:6', '2:6')
    wh.add_passway('2:6', '3:6')
    wh.add_passway('3:6', '4:6')
    wh.add_passway('4:6', '5:6')
    wh.add_passway('5:6', '6:6')
    wh.add_passway('6:6', '7:6')
    # row 6 to row 7
    wh.add_passway('4:6', '4:7')
    return wh


class Command(str, Enum):
    RESET_STATE = "reset_state"
    CAPACITY = "capacity"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('command', choices=[Direction.NORTH, Direction.EAST, Direction.SOUTH,
                        Direction.WEST, Command.RESET_STATE, Command.CAPACITY], help='Command to the robot')
    args = parser.parse_args()

    robot = Robot(requester=Requester(), apiURL=WAREHOUSE_API_URL, sessionID=SESSION_ID)

    try:
        robot = robot.load("robot")
    except Exception:
        robot = Robot(requester=Requester(), apiURL=WAREHOUSE_API_URL, sessionID=SESSION_ID)

    robot.warehouse = create_large_warehouse()

    if (args.command == Direction.NORTH or args.command == Direction.EAST or args.command == Direction.SOUTH or args.command == Direction.WEST):
        response = robot.move(args.command)
        if (response["request_status"] == "ok"):
            print("Successful")
        else:
            print("Failed: " + response["message"])
    elif (args.command == Command.RESET_STATE):
        response = robot.resetState()
        if (response["request_status"] == "ok"):
            print("Successful")
        else:
            print("Failed: " + response["message"])
    elif (args.command == Command.CAPACITY):
        print(robot.warehouse.overall_capacity())

    robot.save("robot")

    robot.warehouse.plotter.draw(
        robot.warehouse.plotter.graph_to_nodes(robot.warehouse.layout),
        str(robot.position["x"]) + ":" + str(robot.position["y"]))
    robot.warehouse.plotter.show()


if __name__ == "__main__":
    main()
